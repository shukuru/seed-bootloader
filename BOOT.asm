;; Seed bootloader - Hobbyist x86 bootloader for the TulipOS Project
;; Copyright (C) 2021  DUPONT Florent
;; 
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; BOOT.asm - Seed bootloader execution source file

BITS 16
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bootloader macro-definitions ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
%define SEED_EXEC_OFFSET      0x0800
%define SEED_BDA_OFFSET       0x0c00
%define SEED_MLA_OFFSET       0x1000
%define BULB_KERNEL_OFFSET    0x100000

;; Execution origin offset
ORG SEED_EXEC_OFFSET

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bootloader execution ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
BOOT_start:
	pop dx
	push BOOT_LOADING_KERNEL_MSG
	call io_print

	jmp $
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bootloader subroutines ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
%include "seed_IO.asm"

;;;;;;;;;;;;;;;;;;;;;;;;
;; Bootloader strings ;;
;;;;;;;;;;;;;;;;;;;;;;;;

;; Data starts at SEED BDA offset
times SEED_BDA_OFFSET-($-$$) db 0x00
	
BOOT_LOADING_KERNEL_MSG:    db "Loading Bulb kernel...", 0x0a, 0x0d, 0x00

;; Data ends at SEED MLA offset
times SEED_MLA_OFFSET-($-$$) db 0x00	
