;; Seed bootloader - Hobbyist x86 bootloader for the TulipOS Project
;; Copyright (C) 2021  DUPONT Florent
;; 
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; MBR.asm - Seed bootloader MBR source file

BITS 16
;;;;;;;;;;;;;;;;;;;;;;;
;; Seed version info ;;
;;;;;;;;;;;;;;;;;;;;;;;
	
;; Version info is embedded by Makefile and should not be manually edited
%define SEED_MAJOR 0
%define SEED_MINOR 0
%define SEED_BUILD 1

;;;;;;;;;;;;;;;;;;;;
;; MBR memory map ;;
;;;;;;;;;;;;;;;;;;;;
%define STACK_POINTER       0x05ff
%define MBR_LOAD_OFFSET     0x7c00
%define MBR_RELOC_OFFSET    0x0600

;;;;;;;;;;;;;;;;;;;;;;;;
;; MBR macrofunctions ;;
;;;;;;;;;;;;;;;;;;;;;;;;

;; Print bootloader version info
%macro PrintVersionInfo 0
	mov si, MBR_STRINGS.versionInfo
	call Print
%endmacro

;; Get FAT root directories total entries size
%macro GetFATRootDirSize 0
	mov ax, 0x20		                     ; 32-bytes directory entry
	mul WORD [FAT_EBPB.NumberOfRootDirEntries]   ; Directory entry size * total entries number
	div WORD [FAT_EBPB.SectorSize]	             ; Divided by sector size in bytes
%endmacro

;; Get FAT root directory offset
%macro GetFATRootDirOffset 0
	mov ax, WORD [FAT_EBPB.NumberOfFATs]
	mul WORD [FAT_EBPB.SectorsPerFAT]            ; Number of FATs * Sectors per FAT
	add ax, WORD [FAT_EBPB.ReservedSectors]	     ; Add reserved sectors
	mul [FAT_EBPB.SectorSize]		     ; Multiply by sector size in bytes
%endmacro

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FAT short jump three bytes instruction ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
jmp short MBR_segstack
nop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FAT12/16 Extended BIOS Parameter Block ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FAT_EBPB:
.OEMLabel:               db "mkdosfs", 0x00 ; OEM label, format program identifier
.SectorSize:             dw 512		    ; Sector size in bytes
.SectorsPerCluster:      db 1		    ; Sector count per cluster
.ReservedSectors:        dw 1		    ; Reserved sectors out of filesystem visibility
.NumberOfFATs:           db 2		    ; FAT count on disk
.NumberOfRootDirEntries: dw 224		    ; Total count of root directory entries
.LogicalSectors:         dw 2880	    ; Total count of sectors on disk
.DiskIdentifier:         db 0xf0	    ; Disk identifier code
.SectorsPerFAT:          dw 9		    ; Sectors used per FAT
.SectorsPerTrack:        dw 18		    ; Sector count per Track
.NumberOfHeads:          dw 2		    ; Total count of disk Heads
.HiddenSectors:          dd 0		    ; Total count of hidden sectors
.LargeSectors:           dd 0		    ; Total count of large sectors
.DriveNumber:            db 0		    ; BIOS returned drive code
.NTFlag:                 db 0		    ; Reserved byte for Windows NT
.Signature:              db 0x29	    ; Signature
.VolumeID:               dd 0		    ; Volume serial number
.VolumeLabel:            db "SEED001    "   ; Space padding (11 bytes)
.FileSystem:             db "FAT16   "	    ; Space padding (8 bytes)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BOOTLOADER 16-BIT SEGMENTATION AND STACK SETUP ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MBR_segstack:
	cli			          ; Clear interrupts

	xor ax, ax		          ; Init. ax register
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov sp, STACK_POINTER		  ; Stack pointer 1 byte below relocated MBR (0x600)

	cld			          ; Clear direction flag

	push dx			          ; Save drive code to stack

	jmp short MBR_reloc
;;;;;;;;;;;;;;;;;;;;
;; MBR RELOCATION ;;
;;;;;;;;;;;;;;;;;;;;
MBR_reloc:
	mov si, MBR_LOAD_OFFSET	         ; Put BIOS load offset
	mov di, MBR_RELOC_OFFSET         ; Put relocation address offset
	mov cx, MBR_BOOTSIGNATURE        ; Copy code & data bytes of MBR (No need to copy padding bytes and boot signature)

	rep movsb		         ; Copy CX bytes from ds:[si] to es:[di]

	jmp 0x0:MBR_exec	         ; Far jump to relocated execution code

;;;;;;;;;;;;;;;;;;;;;;;;
;; MBR EXECUTION CODE ;;
;;;;;;;;;;;;;;;;;;;;;;;;
ORG 0x600
MBR_exec:
	pop dx

	cmp dl, [FAT_EBPB.DriveNumber]    ; Check for corresponding EBPB and BIOS drive code
	jne MBR_errorstate.bad_drive_code ; Display error if not
	
	call PrintNewline
	PrintVersionInfo
	call PrintNewline

.load_boot_partition:
	mov si, MBR_STRINGS.loadingMessage
	call Print
	hlt

;;;;;;;;;;;;;;;;;;;;;
;; MBR ERROR STATE ;;
;;;;;;;;;;;;;;;;;;;;;
MBR_errorstate:
.bad_drive_code:
	mov si, ERRORS.ebaddrive
	call Print
	jmp short .halt

.halt
	cli
	hlt
	

;;;;;;;;;;;;;;;;;;;;;
;; MBR subroutines ;;
;;;;;;;;;;;;;;;;;;;;;
Print:
	; PURPOSE:
	;    Prints a null-terminated byte string
	;
	; PARAMETERS:
	;    ds:si    String address
	;
	; LOCALS:
	;    none
	;
	; RETURNS:
	;    none
	mov ah, 0x0e
	xor bh, bh
.print_string:
	lodsb
	or al, al		; Check for null byte char
	jz .exit
	int 0x10
	jmp .print_string
.exit:
	ret

PrintNewline:
	; PURPOSE:
	;    Prints a new line
	;
	; PARAMETERS:
	;    none
	;
	; LOCALS:
	;    none
	;
	; RETURNS:
	;    none
	mov si, MBR_STRINGS.newline
	call Print
	ret

;;;;;;;;;;;;;;;;;
;; MBR STRINGS ;;
;;;;;;;;;;;;;;;;;
MBR_STRINGS:
.versionInfo:                   db    "Seed Bootloader - Version ", (SEED_MAJOR+0x30), 0x2e, (SEED_MINOR+0x30), 0x2e, (SEED_BUILD+0x30), 0x0a, 0x0d, 0x00 ; Embed version tag
.newline:                       db    0x0a, 0x0d, 0x00 ; "\n\r\0"
.loadingMessage:                db    "Loading...", 0x0a, 0x0d, 0x00
.bootloaderFilename:            db    "BOOT    BIN" ; Space padded FAT filename

ERRORS:
.ebaddrive:                     db    "ERROR: Drive code differs from BIOS", 0x0a, 0x0d, 0x00
	
;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BOOTSECTOR SIGNATURE ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
MBR_BOOTSIGNATURE:
times 0x01f0+0x0e-($-$$) db 0x0
dw 0xaa55
