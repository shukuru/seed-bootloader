# Seed bootloader - Hobbyist x86 bootloader for the TulipOS Project
# Copyright (C) 2021  DUPONT Florent
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Makefile - Seed bootloader Makefile (Make recipe)

# Colors :)
FC_DEFAULT=\e[39;0m
FC_BOLD=\e[39;1m
FC_RED=\e[91;1m
FC_GREEN=\e[92;1m
FC_YELLOW=\e[93;1m
FC_BLUE=\e[94;1m
FC_MAGENTA=\e[95;1m
FC_CYAN=\e[96;1m

# Environment variables
VERSION_MAJOR=0
VERSION_MINOR=0
VERSION_BUILD=1

DISPLAY_DUMP=false

BSIZE=512

FLOPPY_TOT_BLOCKS=2880

# Toolchain variables
ASM=nasm
ASM_FLAGS=-O0 -f bin

MBR=MBR
STAGE2=BOOT
STAGE3=KERNEL

DISK_IMG=seed-$(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_BUILD)

# Targets
.PHONY: clean mrproper

usbdrive:
# TODO
	@echo -e "$(FC_MAGENTA)[Target $@ not implemented yet]\n$(FC_DEFAULT)"

harddrive:
# TODO
	@echo -e "$(FC_MAGENTA)[Target $@ not implemented yet]\n$(FC_DEFAULT)"

floppy: mbr stage2
	@echo -e "$(FC_YELLOW)### Building Floppy disk image ###\n$(FC_DEFAULT)"

	@echo -e "\t$(FC_MAGENTA)+$(FC_DEFAULT) Creating FAT12 file system in $(DISK_IMG).flp"
	@mkfs.fat -F 12 -C $(DISK_IMG).flp 1440

	@echo -e "\t$(FC_MAGENTA)+$(FC_DEFAULT) Overwriting with Seed Master Boot Record..."
	@dd if=MBR.BIN of=$(DISK_IMG).flp conv=notrunc bs=512 count=1 seek=0

	@echo -e "\t$(FC_MAGENTA)+$(FC_DEFAULT) Mounting filesystem..."
	@mkdir -pv /media/floppy
	@mount -v -o loop $(DISK_IMG).flp /media/floppy

	@echo -e "\t$(FC_MAGENTA)+$(FC_DEFAULT) Copying $(STAGE2).BIN on filesystem"
	@cp -v $(STAGE2).BIN /media/floppy

	@echo -e "\t$(FC_MAGENTA)+$(FC_DEFAULT) Unmounting filesystem..."
	@umount -v /media/floppy

	@echo -e "\n$(FC_GREEN)[Done]\n$(FC_DEFAULT)"

ifeq ($(DISPLAY_DUMP),true)
	@echo -e "$(FC_MAGENTA)-0x- Displaying $@ dump -0x-$(FC_DEFAULT)\n"
	@hexdump -C $(DISK_IMG).flp
	@echo -e "\n"
endif

stage3:
	@echo -e "$(FC_MAGENTA)[Target $@ not implemented yet]\n$(FC_DEFAULT)"
stage2:
	@echo -e "$(FC_YELLOW)### Building Bootloader ###\n$(FC_DEFAULT)"
	$(ASM) $(ASM_FLAGS) $(STAGE2).asm -o $(STAGE2).BIN
	@echo -e "\n$(FC_GREEN)[Done]\n$(FC_DEFAULT)"

ifeq ($(DISPLAY_DUMP),true)
	@echo -e "$(FC_MAGENTA)-0x- Displaying $@ dump -0x-$(FC_DEFAULT)\n"
	@hexdump -C $(STAGE2).BIN
	@echo -e "\n"
endif
mbr:
	@echo -e "$(FC_YELLOW)### Building Master Boot Record ###\n$(FC_DEFAULT)"
	@sed -i -E 's/SEED_MAJOR\ [0-9]+/SEED_MAJOR\ $(VERSION_MAJOR)/g' $(MBR).asm
	@sed -i -E 's/SEED_MINOR\ [0-9]+/SEED_MINOR\ $(VERSION_MINOR)/g' $(MBR).asm
	@sed -i -E 's/SEED_BUILD\ [0-9]+/SEED_BUILD\ $(VERSION_BUILD)/g' $(MBR).asm
	$(ASM) $(ASM_FLAGS) $(MBR).asm -o $(MBR).BIN
	@echo -e "\n$(FC_GREEN)[Done]\n$(FC_DEFAULT)"

ifeq ($(DISPLAY_DUMP),true)
	@echo -e "$(FC_MAGENTA)-0x- Displaying $@ dump -0x-$(FC_DEFAULT)\n"
	@hexdump -C $(MBR).BIN
	@echo -e "\n"
endif

clean:
	@echo -e "$(FC_CYAN)### Cleaning previous build files ###$(FC_DEFAULT)\n"
	@find . -type f -name "*.BIN" -print -exec rm -f {} \;
	@find . -type f -name "$(DISK_IMG)*" -print -exec rm -f {} \;
	@echo -e "\n$(FC_GREEN)[Done]\n$(FC_DEFAULT)"

mrproper:
	@echo -e "$(FC_CYAN)### Cleaning GNU Emacs buffer and working files ###$(FC_DEFAULT)\n"
	@find . -depth -type f -name "*#*#" -print -exec rm -f {} \;
	@find . -depth -type f -name "*~" -print -exec rm -f {} \;
	@echo -e "\n$(FC_GREEN)[Done]\n$(FC_DEFAULT)"
