# 🌱 Seed bootloader
Seed is a hobbyist x86 assembly bootloader, initially made for the [TulipOS project](https://github.com/users/flodup/projects/1)

## Purpose
Seed bootloader follow the same [TulipOS](https://github.com/users/flodup/projects/1) purpose: Learn and gain low-level programming skills.

## Materials
This bootloader is written with GNU Emacs under Fedora 33, tested in Qemu i386 virtual machine and debugged through remote instance of GDB.
As reading support:
+ [Operating Systems: From 0 to 1](https://github.com/tuhdo/os01) book.
+ [Writing a Simple Operating System from Scratch](https://www.cs.bham.ac.uk/~exr/lectures/opsys/10_11/lectures/os-dev.pdf) book.
+ [osdev.org](https://wiki.osdev.org/Main_Page) community.
+ [Writing a Tiny x86 Bootloader](https://www.joe-bergeron.com/posts/Writing%20a%20Tiny%20x86%20Bootloader/)

## Testing
#### Build
If you want to test this bootloader, you have to use the Makefile recipe:

|Target name|Description|
|:---------:|:---------:|
|usbdrive|Make a FAT32 USB disk image file|
|harddrive|Make a FAT32 Hard Disk image file|
|floppy|Make a FAT12 Floppy Disk image file|
|stage3|Make the bootloader stage 3 binary file|
|stage2|Make the bootloader stage 2 binary file|
|mbr|Make the Master Boot Record (stage1) binary file|
|clean|Clean previous build files & binaries|
|mrproper|Clean GNU Emacs buffer & working files|

You can set, at build, the environment variable `DISPLAY_DUMP=true` to displays a canonical hexadecimal dump of each target
if applicable. By default, this variable is set to false.
Displayed dump will squeezes repeating pattern such as padding bytes for a full offset range.

To be used with Qemu, use the following command in your terminal:

**Floppy**
```bash
qemu-system-i386 -machine q35 -drive format=raw,file=build/seed-i386-x.y.z.flp,readonly,index=0,if=floppy
```

**Hard Drive**
```bash
# Coming soon...
```

#### Debugging
If you want to inspect and debug the bootloader execution, add `-gdb tcp::8000 -S` parameters at Qemu command line end, 
then starts a gdb instance and reach the remote target and set correct architecture with:
```bash
target remote localhost:8000
set architecture i8086
set disassembly-flavor intel
```

You can set breakpoints at a specific address by typing `b *0x07c00` (The well-known one for example)

To step between instructions, type `si`

To continue, type `c`

To exit, type `quit`

## Supported filesystems
Actually, the bootloader will only support FAT filesystem, FAT12 for floppy, FAT32 for USB and Hard drives.

## Bootloader Memory Map
You can consult the current [Seed Bootloader Memory Map diagram](https://raw.githubusercontent.com/flodup/seed-bootloader/master/seed-mmap.svg).
Changes will comes further.

## What's next ?
Actually, the bootloader is nothing else than a boot sector that loads a dummy program
Next part will be to launch a program from the bootloader to "complete" the POST->BIOS->Bootloader->Program (Kernel) chain.

It will remain on a floppy format for the moment, upgrading to hard drive image later.
As you can see in [Materials](#Materials) section, I'm studying the subject

## End word
Feel free to contact me for questions or ideas of improvment.
