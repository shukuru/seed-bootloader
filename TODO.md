# General
- [x] Design bootloader memory map
- [x] Memory map diagram for users
- [ ] Support screen print
- [ ] Support FAT filesystem at least
- [ ] Organize bootloader files & micro-executables (Modules like GRUB ?)

# Stage 1 (MBR)
- [x] Disable IRQs to avoid race with ISRs
- [x] Clear segment registers
- [x] Set a 256 bytes stack at `0x0000:0x05ff`
- [x] Set direction flag to 0 (Onward direction for string instructions such as lodsb)
- [x] Store drive code on stack (BIOS put this value in dl register before loading MBR)
- [x] Relocate MBR at `0x0000:0x0600`
- [x] Displays version info
- [ ] Load bootloader (stage 2) at 0x0000:0x0500 (RAM) from 0x200 (Disk) -> Relocation
- [x] Be padded with null bytes until offset 510 & 511 (0x1fe & 0x1ff)
- [x] Boot signature byte sequence (0x55 0xaa) at 0x1fe

# Stage 2 (Bootloader)
- [ ] Look for bootable partition (Supported FS) on the drive it was loaded from (Drive code)
- [ ] Collect usefull informations for kernel (Memory map, Memory size, etc...)
- [ ] GDT, IDT
- [ ] Enable A20 gate
- [ ] Enter 32-bit protected mode
- [ ] Set a proper stack for kernel
- [ ] Locate and load boot partition on supported filesystem (FAT?,Ext4?) at 0x1000:0x0000 (RAM) from disk

# Stage 3 (Kernel incoming!)
- [ ] Load kernel
- [ ] Enter kmain