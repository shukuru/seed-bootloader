;; Seed bootloader - Hobbyist x86 bootloader for the TulipOS Project
;; Copyright (C) 2021  DUPONT Florent
;; 
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; seed_IO.asm - I/O subroutines source file

BITS 16

io_print:
;; PURPOSE: Prints a null-terminated string on screen
;;
;; PARAMETERS:
;;    [sp+4]: String first byte address
;;
;; LOCALS:
;;    cl: Current written characters number
;;
;; RETURNS:
;;    al: Number of characters written
	push bp
	mov bp, sp

	xor cl, cl		; Init. cl register
	mov ah, 0x0e		; Teletype
	xor bh, bh
	mov si, [bp+4]		; Load string address from stack

.io_print_prints:
	lodsb			; Load next string byte
	or al, al 		; Check for null terminator
	jz .io_print_exit	; Exit subroutine if zero flag set
	int 0x10		; Video interrupt service
	add cl, 0x1 		; Count the byte
	jmp .io_print_prints	; Loop

.io_print_exit:
	mov al, cl		; Set the return value register

	mov sp, bp
	pop bp
	ret
